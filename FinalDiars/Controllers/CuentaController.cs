﻿using FinalDiars.DB;
using FinalDiars.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalDiars.Controllers
{


    public class CuentaController : Controller
    {
        private AppFinalContext context;
        public CuentaController(AppFinalContext context)
        {
            this.context = context;
        }

        // GET: CuentaController
        public ActionResult Index()
        {
            var usuario = GetUsuario();

            var cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();


            ViewBag.Propio = CalcularSaldoPropio(cuentas);
            ViewBag.Credito = CalcularSaldoCredito(cuentas);
            return View(cuentas);
        }

        private double CalcularSaldoPropio(List<Cuentas> cuentas) {
            double saldoPropio=0;
            double deuda = 0;
            
            foreach (var item in cuentas)
            {
                if (item.Categoria == "Propia")
                {
                    saldoPropio = saldoPropio + item.Saldo;
                }
                else
                {
                    var gastos = context.Gastos.Where(o => o.CuentaId == item.Id).ToList();
                    var ingresos = context.Ingresos.Where(o => o.CuentaId == item.Id).ToList();
                    foreach (var ingreso in ingresos)
                    {
                        deuda = deuda + ingreso.Monto;
                    }
                    foreach (var gasto in gastos)
                    {
                        deuda = deuda - gasto.Monto;
                    }
                }
            }
            if (deuda < 0)
            {
                saldoPropio = saldoPropio + deuda;
            }


            return saldoPropio;
        }
        private double CalcularSaldoCredito(List<Cuentas> cuentas)
        {
            double saldo = 0;
            foreach (var item in cuentas)
            {
                saldo = saldo + item.Saldo;
             
            }
            return saldo;
        }
        [HttpGet]
        public ActionResult Crear()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Crear(Cuentas cuenta)
        {
            var usuario = GetUsuario();
            cuenta.UsuarioId = usuario.Id;
            context.Cuentas.Add(cuenta);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ListarIngresos(int id)
        {
            var cuenta = context.Cuentas.Where(o => o.Id == id).FirstOrDefault();
            var ingresos = context.Ingresos.Where(o => o.CuentaId == cuenta.Id);
            ViewBag.Cuenta = cuenta;
            return View(ingresos);
        }

        public ActionResult ListarGastos(int id)
        {

            var cuenta = context.Cuentas.Where(o => o.Id == id).FirstOrDefault();
            var gastos = context.Gastos.Where(o => o.CuentaId == cuenta.Id);
            ViewBag.Cuenta = cuenta;
            return View(gastos);
        }

        [HttpGet]
        public ActionResult RegistrarGasto()
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            return View(new Gastos());
        }
        [HttpPost]
        public ActionResult RegistrarGasto(Gastos gasto)
        {
            var cuenta = context.Cuentas.Find(gasto.CuentaId);

            if (gasto.Monto <= cuenta.Saldo) {
                
                cuenta.Saldo = cuenta.Saldo - gasto.Monto;
                context.Gastos.Add(gasto);
                context.SaveChanges();

                return RedirectToAction("ListarGastos", new { id = gasto.CuentaId });
            }
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            ModelState.AddModelError("Monto", "El monto ingresado supera el saldo de la cuenta");
            return View(gasto);

        }

        [HttpGet]
        public ActionResult RegistrarIngreso()
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            
            return View();
        }

        [HttpPost]
        public ActionResult RegistrarIngreso(Ingresos ingreso)
        {
            
            var cuenta = context.Cuentas.Find(ingreso.CuentaId);

            cuenta.Saldo = cuenta.Saldo + ingreso.Monto;
            context.Ingresos.Add(ingreso);
            context.SaveChanges();
            return RedirectToAction("ListarIngresos",new {id = ingreso.CuentaId });
        }
        [HttpGet]
        public ActionResult TransferenciaCuentas()
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            return View(0);
        }

        [HttpPost]
        public ActionResult TransferenciaCuentas(float monto, int idCuenta1, int idCuenta2, DateTime fecha)
        {
            var cuentaOrigen = context.Cuentas.Find(idCuenta1);
            if (monto <= cuentaOrigen.Saldo) {
                var cuentaDestino = context.Cuentas.Find(idCuenta2);
                
                var gasto = new Gastos() { CuentaId = idCuenta1, 
                    FechaHora = fecha,
                    Monto = monto,
                    Descripcion = "Transferencia" };
                var ingreso = new Ingresos() { CuentaId = idCuenta2, 
                    FechaHora=fecha,
                    Monto = monto,
                    Descripcion = "Transferencia" };
                cuentaOrigen.Saldo = cuentaOrigen.Saldo - monto;
                cuentaDestino.Saldo = cuentaDestino.Saldo + monto;
                context.Gastos.Add(gasto);
                context.Ingresos.Add(ingreso);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            ModelState.AddModelError("Monto", "El monto ingresado supera el saldo de la cuenta");
            return View(monto);
        }
        [HttpGet]
        public ActionResult EnviarDineroContacto(int id)
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            ViewBag.cuentasContacto = context.Cuentas.Where(o => o.UsuarioId == id).ToList();

            return View(0);
        }
        [HttpPost]
        public ActionResult EnviarDineroContacto(float monto, int idMiCuenta, int idCuentaContacto, DateTime fecha)
        {
            var cuentaOrigen = context.Cuentas.Find(idMiCuenta);
            if (monto <= cuentaOrigen.Saldo)
            {
                var cuentaDestino = context.Cuentas.Find(idCuentaContacto);

                var gasto = new Gastos()
                {
                    CuentaId = idMiCuenta,
                    FechaHora = fecha,
                    Monto = monto,
                    Descripcion = "Envio de dinero a contacto"
                };
                var ingreso = new Ingresos()
                {
                    CuentaId = idCuentaContacto,
                    FechaHora = fecha,
                    Monto = monto,
                    Descripcion = "Envio de dinero a contacto"
                };
                cuentaOrigen.Saldo = cuentaOrigen.Saldo - monto;
                cuentaDestino.Saldo = cuentaDestino.Saldo + monto;
                context.Gastos.Add(gasto);
                context.Ingresos.Add(ingreso);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            ModelState.AddModelError("Monto", "El monto ingresado supera el saldo de la cuenta");
            return View(monto);
        }
        public ActionResult ListarUsuarios()
        {
            var usuarios = context.Usuarios.ToList();
            ViewBag.Usuario = GetUsuario();
            return View(usuarios);
        }
        public ActionResult ListarContactos()
        {
            
            ViewBag.Solicitudes = GetSolicitudes();
            ViewBag.Contactos = GetContactos();
            return View();
        }

        public ActionResult EnviarSolicitud(int id)
        {
            var contacto = context.Usuarios.Where(o=>o.Id==id).FirstOrDefault();
            var usuario = GetUsuario();
            
            var amistad = new Contactos() {UsuarioId = usuario.Id,
                ContactoId = contacto.Id,
                EstadoSolicitud = "En espera"
            };
            context.Contactos.Add(amistad);
            context.SaveChanges();
            ViewBag.Usuario = GetUsuario();
            return RedirectToAction("Index");
        }
        public ActionResult AceptarSolicitud(int id)
        {
            var usuario = GetUsuario();
            var solicitud = context.Contactos.Where(o => o.ContactoId == usuario.Id && o.UsuarioId == id).FirstOrDefault();
            solicitud.EstadoSolicitud = "Aceptada";
            context.SaveChanges();
            return RedirectToAction("ListarContactos");
        }
        private Usuarios GetUsuario()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            if (claim != null)
            {
                var user = context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
                return user;
            }

            return null;
        }
        private List<Usuarios> GetSolicitudes() {
            var usuario = GetUsuario();
            var solicitudes = context.Contactos.Where(o => o.ContactoId == usuario.Id && o.EstadoSolicitud == "En espera").ToList();
            var listaSolicitudes = new List<Usuarios>();
            foreach (var item in solicitudes)
            {
                listaSolicitudes.Add(context.Usuarios.Find(item.UsuarioId));
            }
            return listaSolicitudes;
        }
        private List<Usuarios> GetContactos()
        {
            var usuario = GetUsuario();
            
            var a = context.Contactos.Where(o => o.ContactoId == usuario.Id && o.EstadoSolicitud == "Aceptada").ToList();
            var b = context.Contactos.Where(o => o.UsuarioId == usuario.Id && o.EstadoSolicitud == "Aceptada").ToList();

            var listaContactos = new List<Usuarios>();
            var usuarios = context.Usuarios.ToList();


            foreach (var item in a)
            {
                listaContactos.Add(context.Usuarios.Find(item.UsuarioId));
            }
            foreach (var item in b)
            {
                listaContactos.Add(context.Usuarios.Find(item.ContactoId));
            }

            return listaContactos;
        }

    }
}
