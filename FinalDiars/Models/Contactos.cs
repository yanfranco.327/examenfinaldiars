﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalDiars.Models
{
    public class Contactos
    {
        public int Id { get; set; }
        public int ContactoId { get; set; }
        public int UsuarioId { get; set; }
        public string EstadoSolicitud { get; set; }
        public Usuarios Usuario { get; set; }
    }
}
