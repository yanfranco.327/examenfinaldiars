﻿using FinalDiars.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalDiars.DB.Maps
{
    public class ContactoMaps : IEntityTypeConfiguration<Contactos>
    {
        public void Configure(EntityTypeBuilder<Contactos> builder)
        {
            builder.ToTable("Contacto");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.Usuario).WithMany().HasForeignKey(o=>o.ContactoId);
        }
    }
}
