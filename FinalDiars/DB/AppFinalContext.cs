﻿using FinalDiars.DB.Maps;
using FinalDiars.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalDiars.DB
{
    public class AppFinalContext : DbContext
    {
        public DbSet<Usuarios> Usuarios{ get; set; }
        public DbSet<Cuentas> Cuentas { get; set; }
        public DbSet<Ingresos> Ingresos { get; set; }
        public DbSet<Gastos> Gastos { get; set; }
        public DbSet<Contactos> Contactos { get; set; }

        public AppFinalContext(DbContextOptions<AppFinalContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UsuarioMaps());
            modelBuilder.ApplyConfiguration(new CuentaMaps());
            modelBuilder.ApplyConfiguration(new GastoMaps());
            modelBuilder.ApplyConfiguration(new IngresoMaps());
            modelBuilder.ApplyConfiguration(new ContactoMaps());

        }
    }
}
